﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Business.DomainModel;

namespace Business.Data
{
    public interface IUnitOfWork
    {
        IRepository<Restaurant> Restaurants { get; }

        void UpdateRestaurant(Restaurant restaurant);

        void Commit();
    }
}

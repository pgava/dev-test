﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Business.Data
{
    public interface IRepository<T>
        where T : class, IEntity
    {
        void Add(T entity);
        void Remove(T entity);

        IQueryable<T> Find(Expression<Func<T, bool>> predicate);
        IQueryable<T> FindAll();

        T FindById(int id);
         
    }
}

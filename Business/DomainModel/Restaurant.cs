﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Business.Data;
using System.Text.RegularExpressions;

namespace Business.DomainModel
{
    public class Restaurant : IEntity
    {
        public int Id { get; set; }
        public string Title { get; set; }

        /// <summary>
        /// Should always be stored in format: (00) 0000 0000
        /// </summary>
        public string PhoneNumberText { get; set; }

        /// <summary>
        /// Phone number as entered into UI is converted to a long here, for use as a natural key
        /// </summary>
        public long PhoneNumberKey { get; set; }

        public CuisineEnum Cuisine { get; set; }
        public string HeadChefName { get; set; }
        
        /// <summary>
        /// "star-ratings" for food/wine must be in range 0 - 3
        /// </summary>
        public byte? RatingFood { get; set; }
        public byte? RatingWine { get; set; }

        public string StreetAddress { get; set; }
        public string AddressSuburb { get; set; }
        public string AddressState { get; set; }
        public string AddressPostCode { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public string WebAddress { get; set; }
        public string ReviewText { get; set; }

        public bool TryCreateRestaurantForUpdate()
        {
            var phone = Regex.Replace(PhoneNumberText, @"[^\d]", "");
            if (phone.Length == 10)
            {
                var chunks = Split(phone, 2).ToArray();
                PhoneNumberText = string.Format("({0}) {1}{2} {3}{4}", chunks[0], chunks[1], chunks[2], chunks[3], chunks[4]);
                long phoneKey = 0;
                if (long.TryParse(phone, out phoneKey))
                {
                    PhoneNumberKey = phoneKey;
                }

                return true;
            }
            return false;
        }

        private IEnumerable<string> Split(string str, int chunkSize)
        {
            return Enumerable.Range(0, str.Length / chunkSize)
                .Select(i => str.Substring(i * chunkSize, chunkSize));
        }
    }
}

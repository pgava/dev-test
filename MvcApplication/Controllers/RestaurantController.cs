﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Business.DomainModel;
using MvcApplication.Models;
using Business.Data;

namespace MvcApplication.Controllers
{
    public class RestaurantController : BaseController
    {
        public RestaurantController(IUnitOfWork uow)
            : base(uow)
        {
        }

        //
        // GET: /Restaurant/

        public ActionResult Edit(int id)
        {
            var restaurant = _uow.Restaurants.FindById(id);
            return View(restaurant);
        }

        [HttpPost]
        public ActionResult Edit(int id, Restaurant restaurant)
        {
            if (ModelState.IsValid)
            {
                if (restaurant.TryCreateRestaurantForUpdate())
                {
                    _uow.UpdateRestaurant(restaurant);

                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("", "Could not update restaurant, please check that all the fields are correct.");
                }

            }

            return View(restaurant);
        }

    }
}

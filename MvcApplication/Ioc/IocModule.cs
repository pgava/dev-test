﻿using Ninject.Modules;
using System.Web;
using Business.Data;
using Infrastructure.EF;

namespace MvcApplication.Ioc
{
    public class IocModule : NinjectModule 
    { 
        public override void Load() 
        {
            Bind<IUnitOfWork>().To<SqlUnitOfWork>()
                .InScope(ctx => HttpContext.Current);
        } 
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using Business.Data;

namespace Infrastructure.EF
{
    public class SqlRepository<T> : IRepository<T>
        where T : class, IEntity
    {
        private ObjectSet<T> _objectSet;
        public SqlRepository(ObjectContext context)
        {
            _objectSet = context.CreateObjectSet<T>();
        }

        public void Add(T entity)
        {
            _objectSet.AddObject(entity);
        }

        public void Remove(T entity)
        {
            _objectSet.DeleteObject(entity);
        }

        public IQueryable<T> Find(System.Linq.Expressions.Expression<Func<T, bool>> predicate)
        {
            return _objectSet.Where(predicate);
        }

        public IQueryable<T> FindAll()
        {
            return _objectSet.AsQueryable();
        }

        public T FindById(int id)
        {
            return _objectSet.Single(o => o.Id == id);
        }
    }
}

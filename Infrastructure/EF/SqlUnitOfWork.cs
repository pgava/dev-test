﻿using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Configuration;
using Business.Data;
using Business.DomainModel;
using System.Data.SqlClient;

namespace Infrastructure.EF
{
    public class SqlUnitOfWork : IUnitOfWork
    {
        private ObjectContext _context;
        private SqlRepository<Restaurant> _restaurants;
        private DataEntities _contextEntities;

        public SqlUnitOfWork()
        {
            _contextEntities = new DataEntities();
            _context = _contextEntities.GetObjectContext();
            _context.ContextOptions.LazyLoadingEnabled = true;
        }

        public IRepository<Restaurant> Restaurants
        {
            get { return _restaurants ?? (_restaurants = new SqlRepository<Restaurant>(_context)); }
        }

        public void Commit()
        {
            _context.SaveChanges();
        }

        public void UpdateRestaurant(Restaurant restaurant)
        {
            var id = new SqlParameter {
                     ParameterName = "id",
                     Value = restaurant.Id
            };

            var title = new SqlParameter
            {
                ParameterName = "title",
                Value = restaurant.Title
            };

            var phoneText = new SqlParameter
            {
                ParameterName = "phone_number_text",
                Value = restaurant.PhoneNumberText
            };

            var phoneKey = new SqlParameter
            {
                ParameterName = "@phone_number_key",
                Value = restaurant.PhoneNumberKey
            };

            _contextEntities.Database.ExecuteSqlCommand("exec UpdateRestaurant @id, @title, @phone_number_text, @phone_number_key ", id, title, phoneText, phoneKey);

        }
    }
}

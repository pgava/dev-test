﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration.Conventions;

using Business.DomainModel;

namespace Infrastructure.EF
{
    public class DataEntities : DbContext
    {
        public DbSet<Restaurant> Restaurants { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<Restaurant>()
                .Property(r =>r.Cuisine)
                .HasColumnName("CuisineId");

        }
    }
}

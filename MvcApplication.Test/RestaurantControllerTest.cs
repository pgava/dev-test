﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using Business.DomainModel;
using MvcApplication.Test.Fake;
using Business.Data;
using MvcApplication.Controllers;
using System.Web.Mvc;
using MvcApplication.Models;

namespace MvcApplication.Test
{
    [TestClass]
    public class RestaurantControllerTest
    {
        [TestMethod]
        public void Should_Be_Able_To_Get_The_Right_Restaurant_For_Editing()
        {
            var listOfRestaurant = new List<Restaurant> 
            { 
                new Restaurant { Id = 1, Title = "Quay", PhoneNumberText = "111" } ,
                new Restaurant { Id = 2, Title = "Rockpool", PhoneNumberText = "222" } ,
                new Restaurant { Id = 3, Title = "Sepia", PhoneNumberText = "333" } ,
            };
            var restaurants = new InMemoryRepository<Restaurant>(listOfRestaurant);

            var moqUow = new Mock<IUnitOfWork>();
            moqUow.Setup(m => m.Restaurants).Returns(restaurants);

            var sut = new RestaurantController(moqUow.Object);

            var model = (ViewResult)sut.Edit(2);

            Restaurant r = model.Model as Restaurant;

            Assert.IsNotNull(r);
            Assert.AreEqual("Rockpool", r.Title);
        }

        [TestMethod]
        public void Should_Be_Able_To_Initialize_A_Restaurant_When_Valid_Input()
        {
            var restaurant = new Restaurant { Id = 1, Title = "a", PhoneNumberText = "(02) 1234 1234" };

            var res = restaurant.TryCreateRestaurantForUpdate();

            Assert.IsTrue(res);
            Assert.AreEqual("a", restaurant.Title);
            Assert.AreEqual(1, restaurant.Id);
            Assert.AreEqual("(02) 1234 1234", restaurant.PhoneNumberText);
            Assert.AreEqual(212341234, restaurant.PhoneNumberKey);

        }

        [TestMethod]
        public void Should_Not_Initialize_A_Restaurant_When_Input_Has_Too_Many_Numbers()
        {
            var restaurant = new Restaurant { Id = 1, Title = "a", PhoneNumberText = "(02) 1234 1234 5" };

            var res = restaurant.TryCreateRestaurantForUpdate();

            Assert.IsFalse(res);

        }

        [TestMethod]
        public void Should_Not_Initialize_A_Restaurant_When_Input_Has_Not_Enough_Numbers()
        {
            var restaurant = new Restaurant { Id = 1, Title = "a", PhoneNumberText = "(02) 1234 123" };

            var res = restaurant.TryCreateRestaurantForUpdate();

            Assert.IsFalse(res);
        }

        [TestMethod]
        public void Should_Be_Able_To_Initialize_A_Restaurant_And_Format_Phone_Number_Correctly()
        {
            var restaurant = new Restaurant { Id = 1, Title = "a", PhoneNumberText = "(02) (1 2 3 4) (12 3 4 )" };

            var res = restaurant.TryCreateRestaurantForUpdate();

            Assert.IsTrue(res);
            Assert.AreEqual("a", restaurant.Title);
            Assert.AreEqual(1, restaurant.Id);
            Assert.AreEqual("(02) 1234 1234", restaurant.PhoneNumberText);
            Assert.AreEqual(212341234, restaurant.PhoneNumberKey);

        }
    }
}

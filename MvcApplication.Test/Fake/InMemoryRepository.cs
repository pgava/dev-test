﻿using Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace MvcApplication.Test.Fake
{
    public class InMemoryRepository<T> : IRepository<T>
        where T : class, IEntity
    {
        private HashSet<T> _set;
        private IQueryable<T> _queryableSet;
        public InMemoryRepository() : this(Enumerable.Empty<T>())
        {            
        }

        public InMemoryRepository(IEnumerable<T> entities)
        {
            _set = new HashSet<T>();
            foreach (var entity in entities)
            {
                _set.Add(entity);
            }

            _queryableSet = _set.AsQueryable();
        }

        public void Add(T newEntity)
        {
            _set.Add(newEntity);
        }

        public void Remove(T entity)
        {
            _set.Remove(entity);
        }

        public IQueryable<T> Find(System.Linq.Expressions.Expression<Func<T, bool>> pre)
        {
            return _queryableSet.Where(pre);
        }

        public IQueryable<T> FindAll()
        {
            return _queryableSet.AsQueryable();
        }

        public T FindById(int id)
        {
            return _queryableSet.Single(o => o.Id == id);
        }
    }
}

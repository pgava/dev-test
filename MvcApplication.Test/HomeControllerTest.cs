﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using Business.DomainModel;
using MvcApplication.Test.Fake;
using Business.Data;
using MvcApplication.Controllers;
using System.Web.Mvc;
using MvcApplication.Models;

namespace MvcApplication.Test
{
    [TestClass]
    public class HomeControllerTest
    {
        [TestMethod]
        public void Should_Be_Able_To_Get_All_The_Restaurants_From_Repository()
        {
            var listOfRestaurant = new List<Restaurant> 
            { 
                new Restaurant { Id = 1, Title = "Quay", PhoneNumberText = "111" } ,
                new Restaurant { Id = 2, Title = "Rockpool", PhoneNumberText = "222" } ,
                new Restaurant { Id = 3, Title = "Sepia", PhoneNumberText = "333" } ,
            };
            var restaurants = new InMemoryRepository<Restaurant>(listOfRestaurant);

            var moqUow = new Mock<IUnitOfWork>();
            moqUow.Setup(m => m.Restaurants).Returns(restaurants);

            var sut = new HomeController(moqUow.Object);

            var model = (ViewResult)sut.Index();

            IEnumerable<Restaurant> r = model.Model as IEnumerable<Restaurant>;

            Assert.IsNotNull(r);
            Assert.AreEqual(3, r.Count());
        }
    }
}
